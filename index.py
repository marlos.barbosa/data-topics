from funcoes import get_heaviest_ox
from funcoes import get_leanest_ox
from funcoes import oxen_registration
import json

with open("dataframe.json", "r") as file:
    data = json.load(file)

x = 0
while x !=2:
    what_to_do = int(input("1.Registrar novo boi\n2.Calcular boi mais magro\n3.Calcular boi mais gordo\n"))
    if what_to_do == 1:
        x = oxen_registration.register_ox(data)
    elif what_to_do == 2:
        x = get_leanest_ox.get_lean(data)
    elif what_to_do == 3:
        x = get_heaviest_ox.get_heavy(data)

print("Programa encerrado!")