import json


def register_ox(info):
    ox_information = {
        "code":None,
        "name":None,
        "weight":None
    }
    one_ox_data = [str(i) for i in input("Dados do boi:").split()]
    ox_information["code"] = one_ox_data[0]
    ox_information["name"] = one_ox_data[1]
    ox_information["weight"] = int(one_ox_data[2])
    info.append(ox_information)
    continuity = int(input("1.Realizar nova ação\n2.Parar o programa\n"))
    with open("dataframe.json", "w") as file:
        json.dump(info,file)
    return continuity


